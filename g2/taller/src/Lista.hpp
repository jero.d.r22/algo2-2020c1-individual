#include "Lista.h"

Lista::Lista() : _prim(nullptr), _ult(nullptr) {}

Lista::Nodo::Nodo(const int &elem) : valor(elem), next(nullptr), prev(nullptr) {}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    Nodo* h = _prim;
    while (h != nullptr){
        Nodo* hn = h->next;
        delete h;
        h = hn;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    if (this->longitud() > 0){
        Nat size = this->longitud();
        for (Nat j=0;j<size;j++){
            this->eliminar(0);
        }
    }
    Nodo* h = aCopiar._prim;
    while (h != nullptr){
        this->agregarAtras(h->valor);
        h = h->next;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* ad = new Nodo(elem);
    if(this->longitud()==0){
        _prim = ad;
        _ult = ad;
    } else {
        ad->next = this->_prim;
        (this->_prim)->prev = ad;
        this->_prim = ad;
    }
}

void Lista::agregarAtras(const int& elem) {
    Nodo* at = new Nodo(elem);
    if (this->longitud()==0) {
        _prim = at;
        _ult = at;
    } else {
        at->prev = _ult;
        _ult->next = at;
        this->_ult = at;
    }
}

void Lista::eliminar(Nat i) {
    Nodo* n = _prim;
    Nodo* ns = n->next;
    if (i==0){
        _prim = n->next;
        delete n;
    } else {
        for (int j=1; j<=i; j++){
            if (j==i){
                Nodo* k = ns;
                n->next = ns->next;
                delete k;
            } else {
                n = n->next;
                ns = ns->next;
            }
        }
    }
}

int Lista::longitud() const {
    int s = 0;
    Nodo* n = this->_prim;
    while (n != nullptr){
            s++;
            n = n->next;
    }
    return s;
}

const int& Lista::iesimo(Nat i) const {
    Nat j = 0;
    Nodo* n = this->_prim;
    while(j<i){
        n = n->next;
        j++;
    }
    return n->valor;
}

int& Lista::iesimo(Nat i) {
    Nat j = 0;
    Nodo* n = this->_prim;
    while(j<i){
        n = n->next;
        j++;
    }
    return n->valor;
}

ostream Lista::mostrar(ostream& o) {
    // Completar
    o << "[";
    if (longitud()>0) {
        if (longitud() == 1) {
            o << iesimo(0);
        } else {
            for (int i = 0; i < this->longitud() - 1; i++) {
                o << this->iesimo(i) << ", ";
            }
            o << this->iesimo(this->longitud() - 1);
        }
    }
    o << "]" << endl;
}
