#include "Conjunto.h"
template <class T>
Conjunto<T>::Conjunto(): _raiz(nullptr), _cardinal(0) {}

template<class T>
Conjunto<T>::Nodo::Nodo(const T &v): valor(v), izq(nullptr), der(nullptr), padre(nullptr){}

template <class T>
Conjunto<T>::~Conjunto() {
    destruir(_raiz);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave)  {
    Nodo *r = _raiz;
    return perteneceABB(r, clave);
}


template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if (!pertenece(clave)) {
        Nodo *r = _raiz;
        insertarABB(r, clave);
        _cardinal = _cardinal + 1;
    }
}


template <class T>
void Conjunto<T>::remover(const T& clave) {
    if(pertenece(clave)){
        Nodo* nodo = _raiz;
        eliminarABB(nodo, clave);
        _cardinal--;
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* nodo = _raiz;
    return sucesorABB(nodo,clave);
}

template <class T>
const T& Conjunto<T>::minimo() const {
    if (_raiz->izq== nullptr){
        return _raiz->valor;
    } else {
        Nodo* temp = _raiz->izq;
        while (temp->izq != nullptr){
            temp = temp->izq;
        }
        return temp->valor;
    }
}

template <class T>
const T& Conjunto<T>::maximo() const {
    if(_raiz->der == nullptr){
        return _raiz->valor;
    } else {
        Nodo* temp = _raiz->der;
        while (temp->der != nullptr){
            temp = temp->der;
        }
        return temp->valor;
    }
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}


template<class T>
bool Conjunto<T>::esHoja(Nodo* &nodo) {
    return nodo->izq == nullptr && nodo->der == nullptr;
}

template<class T>
bool Conjunto<T>::perteneceABB(Conjunto<T>::Nodo* &nodo, const T &clave) {
    if(nodo==nullptr){
        return false;
    } else {
        if (nodo->valor==clave){
            return true;
        } else {
            if (nodo->valor < clave){
                return perteneceABB(nodo->der, clave);
            } else {
                return perteneceABB(nodo->izq, clave);
            }
        }
    }
}

template<class T>
void Conjunto<T>::insertarABB(Nodo* &r, const T &clave) {
    if(r==nullptr){
        r = new Nodo(clave);
        _raiz = r;
    } else {
        if (clave<r->valor){
            if(r->izq== nullptr){
                Nodo* i = new Nodo(clave);
                i->padre = r;
                r->izq= i;
            } else {
                insertarABB(r->izq,clave);
            }
        } else {
            if(r->der== nullptr){
                Nodo* d = new Nodo(clave);
                d->padre = r;
                r->der = d;
            } else {
                insertarABB(r->der, clave);
            }
        }
    }
}

template<class T>
const T &Conjunto<T>::sucesorABB(Conjunto::Nodo *&nodo, const T &clave) {
    if (nodo->valor==clave){
        if(nodo->der!= nullptr){
            return minimoABB(nodo->der);
        } else {
            Nodo* p = nodo->padre;
            while(p!= nullptr && nodo==p->der){
                nodo = p;
                p = p->padre;
            }
            return p->valor;
        }
    } else {
        if (nodo->valor < clave){
            return sucesorABB(nodo->der,clave);
        } else {
            return sucesorABB(nodo->izq,clave);
        }
    }
}

template<class T>
T &Conjunto<T>::minimoABB(Conjunto::Nodo *&nodo) {
    if (nodo->izq== nullptr){
        return nodo->valor;
    } else {
        Nodo* temp = nodo->izq;
        while (temp->izq != nullptr){
            temp = temp->izq;
        }
        return temp->valor;
    }
}

template<class T>
void Conjunto<T>::destruir(Conjunto::Nodo *nodo) {
    if (nodo!=nullptr) {
        destruir(nodo->izq);
        destruir(nodo->der);
        delete nodo;
    }

}

template<class T>
bool Conjunto<T>::tieneUnSoloHijo(Conjunto::Nodo *&nodo) {
    return (nodo->der == nullptr && nodo->izq != nullptr) || (nodo->der != nullptr && nodo->izq == nullptr);
}

template<class T>
void Conjunto<T>::eliminarABB(Conjunto::Nodo *nodo, const T& clave) {
    if (nodo->valor==clave){
        if(esHoja(nodo)){
            if(nodo != _raiz){
                Nodo* p = nodo->padre;
                if(p->izq==nodo){
                    p->izq = nullptr;
                    delete nodo;
                } else {
                    p->der = nullptr;
                    delete nodo;
                }
            } else {
                _raiz = nullptr;
                delete nodo;
            }
        } else {
            if(tieneUnSoloHijo(nodo)){
                if(nodo!=_raiz){
                    Nodo* p = nodo->padre;
                    if (nodo->izq != nullptr){
                        if(p->izq==nodo){
                            p->izq = nodo->izq;
                            nodo->izq->padre=p;
                            delete nodo;
                        } else {
                            p->der= nodo->izq;
                            nodo->izq->padre=p;
                            delete nodo;
                        }
                    } else {
                        if (p->izq==nodo){
                            p->izq = nodo->der;
                            nodo->der->padre=p;
                            delete nodo;
                        } else {
                            p->der = nodo->der;
                            nodo->der->padre=p;
                            delete nodo;
                        }
                    }
                } else {
                    if (nodo->izq!= nullptr){
                        _raiz = nodo->izq;
                        delete _raiz->padre;
                    } else {
                        _raiz = nodo->der;
                        delete _raiz->padre;
                    }
                }
            } else {
                T sig = siguiente(clave);
                eliminarABB(nodo,sig);
                nodo->valor = sig;
            }
        }
    } else {
        if(nodo->valor < clave){
            nodo=nodo->der;
            eliminarABB(nodo,clave);
        } else {
            nodo=nodo->izq;
            eliminarABB(nodo,clave);
        }
    }
}
