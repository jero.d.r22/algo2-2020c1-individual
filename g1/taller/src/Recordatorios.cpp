#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif

  private:
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia): mes_(mes), dia_(dia) {}

int Fecha::mes() {
    return mes_;
}

int Fecha::dia(){
    return dia_;
}

void Fecha::incrementar_dia() {
    if (dia_ < dias_en_mes(mes_)) {
        dia_++;
    } else {
        dia_ = 1;
        if(mes_ == 12) {
            mes_ = 1;
        } else {
            mes_++;
        }
    }
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = mes() == o.mes();
    return igual_dia && igual_mes;
}
#endif

// Ejercicio 11, 12

class Horario{
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator<(Horario h);

    private:
        uint hora_;
        uint min_;
};

Horario::Horario(uint hora, uint min): hora_(hora), min_(min) {}

uint Horario::hora() {
    return hora_;
}

uint Horario::min() {
    return min_;
}

ostream& operator<<(ostream& os, Horario h){
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator<(Horario h) {
    if(hora_ == h.hora()){
        return min_ < h.min();
    } else {
        return hora_ < h.hora();
    }
}



// Ejercicio 13

class Recordatorio{
    public:
        Recordatorio(Fecha f, Horario h, string msg);
        Fecha f();
        Horario h();
        string msg();
        bool operator<(Recordatorio r);

    private:
        Fecha f_;
        Horario h_;
        string msg_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string msg): f_(f), h_(h), msg_(msg) {}

Fecha Recordatorio::f(){
    return f_;
}

Horario Recordatorio::h(){
    return h_;
}

string Recordatorio::msg(){
    return msg_;
}

ostream& operator<<(ostream& os, Recordatorio r){
    os << r.msg() << " @ " << r.f() << " " << r.h();
    return os;
}

bool Recordatorio::operator<(Recordatorio r) {
    return h_ < r.h();
}
//No tomo en cuenta la fecha ya que este operador solo se usa dentro de un mismo dia.

// Ejercicio 14

class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

    private:
        list<Recordatorio> recordatorios_de_hoy_;
        Fecha hoy_;
        list<Recordatorio> rec_todos_;
};

Agenda::Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial) {}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    rec_todos_.push_back(rec);
}

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy(){
    for(Recordatorio rec: rec_todos_){
        if(rec.f()==hoy_){
            recordatorios_de_hoy_.push_back(rec);
        }
    }
    recordatorios_de_hoy_.sort();
    return recordatorios_de_hoy_;
};

Fecha Agenda::hoy(){
    return hoy_;
}

ostream& operator<<(ostream& os, Agenda a){
    os << a.hoy() << endl;
    os << "=====" << endl;
    for (Recordatorio rec: a.recordatorios_de_hoy()){
        os << rec.msg() << " @ " << rec.f() << " " << rec.h() << endl;
    }
}
