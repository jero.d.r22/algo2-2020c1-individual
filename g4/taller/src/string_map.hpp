
#include "string_map.h"

template <typename T>
string_map<T>::string_map():raiz(nullptr),_size(0){}

template <class T>
string_map<T>::Nodo::Nodo(T* def) : siguientes(256,nullptr),definicion(def){}

template <class T>
string_map<T>::Nodo::Nodo() : siguientes(256,nullptr),definicion(nullptr){}


template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    if(!d.empty()) {
        Nodo *n = new Nodo;
        raiz = n;
        copiar(raiz, d.raiz);
        _size = d._size;
    }
    return *this;
}

template <typename T>
string_map<T>::~string_map(){
    destruir(raiz);
}

template<typename T>
void string_map<T>::insert(const pair<string, T>& par) {
    if (raiz == nullptr) {
        Nodo *t = new Nodo;
        raiz = t;
    }
    Nodo* temp = raiz;
    int i=0;
    int tam=par.first.size();
    while(i<tam && temp->siguientes[int(par.first[i])]!= nullptr){
        temp=temp->siguientes[int(par.first[i])];
        i++;
    }
    if(i==tam){
        if(temp->definicion== nullptr) {
            temp->definicion = new T(par.second);
            _size++;
        } else {
            *temp->definicion = par.second;
        }
    } else {
        while(i<tam){
            Nodo* n = new Nodo;
            temp->siguientes[int(par.first[i])] = n;
            i++;
            temp=n;
        }
        if(temp->definicion== nullptr) {
            temp->definicion = new T(par.second);
            _size++;
        } else {
            *temp->definicion = par.second;
        }
    }
}


template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    int res = 0;
    if (raiz == nullptr){
        return res;
    } else {
        Nodo* temp = raiz;
        int i = 0;
        int tam = clave.size();
        while(i<tam){
            if (temp->siguientes[int(clave[i])]== nullptr){
                return res;
            } else {
                temp=temp->siguientes[int(clave[i])];
                i++;
            }
            if (i==tam){
                if(temp->definicion!=nullptr){
                    res = 1;
                }
            }
        }
    }
    return res;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* temp = raiz;
    int i = 0;
    int tam = clave.size();
    while (i<tam){
        temp = temp->siguientes[int(clave[i])];
        i++;
    }
    return *temp->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* temp = raiz;
    int i = 0;
    int tam = clave.size();
    while (i<tam){
        temp = temp->siguientes[int(clave[i])];
        i++;
    }
    return *temp->definicion;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* temp = raiz;
    Nodo* ultimo = raiz;
    int i = 0;
    int tam = clave.size();
    int ui = 0;
    while (i<tam){
        if (tieneMasDeUnHijo(temp) || temp->definicion!=nullptr){
            ultimo = temp;
            ui = i;
        }
        temp = temp->siguientes[int(clave[i])];
        i++;
    }
    delete temp->definicion;
    temp->definicion= nullptr;
    if (esHoja(temp)){
        Nodo* sig = ultimo->siguientes[int(clave[ui])];
        ultimo->siguientes[int(clave[ui])] = nullptr;
        destruir(sig);
    }
    _size--;

/*    if (ultimo == raiz){
        destruir(raiz);
        raiz=nullptr;
    } else {
        destruir(ultimo);
    }*/
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return _size==0;
}

template<typename T>
void string_map<T>::copiar(string_map::Nodo *n, string_map::Nodo *ac) {
    if (ac->definicion!=nullptr){
        n->definicion = new T(*ac->definicion);
    }
    for(int i = 0; i<256; i++){
        if(ac->siguientes[i]!=nullptr){
            n->siguientes[i] = new Nodo;
            copiar(n->siguientes[i],ac->siguientes[i]);
        }
    }
}

template<typename T>
void string_map<T>::destruir(string_map::Nodo *n) {
    if(n!= nullptr) {
        int tam = n->siguientes.size();
        if (n->definicion != nullptr) {
            delete n->definicion;
        }
        for (int j = 0; j < tam; j++) {
            if (n->siguientes[j] != nullptr) {
                destruir(n->siguientes[j]);
            }
        }
        delete n;
    }
}



template<typename T>
bool string_map<T>::esHoja(string_map::Nodo *n) {
    for(Nodo* temp: n->siguientes){
        if(temp!=nullptr){
            return false;
        }
    }
    return true;
}

template<typename T>
bool string_map<T>::tieneMasDeUnHijo(string_map::Nodo *n) {
    int count = 0;
    for(Nodo* temp: n->siguientes){
        if (temp!= nullptr){
            count++;
        }
    }
    return count > 1;
}
